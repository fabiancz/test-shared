<?php

namespace Shared;

class CPEmail
{
	private $translator;

	public function __construct($translator)
	{
		$this->translator = $translator;
	}

	public function sendMail($to, $body)
	{
		echo $this->translator->translate('sending').$to.' with message: <br>'.$body;
	}
}